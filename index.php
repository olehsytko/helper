<?php

$langs = [
    'PHP',
    'Laravel'
];

$langXml = [];

foreach ($langs as $lang) {
    if (file_exists('xml/' . $lang . '.xml')) {
        $langXml[$lang] = simplexml_load_file('xml/' . $lang . '.xml');
    }
}

//$contents = $langXml['Laravel'];
//
//$content = $contents->addChild('content');
//$content->addChild('title', 'a gallery');
//$content->addChild('body',
//    '
//    DB::enableQueryLog();
//    //sql query
//    dd(DB::getQueryLog());
//    ');
//
//$dom = dom_import_simplexml($langXml['Laravel'])->ownerDocument;
//$dom->formatOutput = true;
//$dom->preserveWhiteSpace = false;
//$dom->loadXML( $dom->saveXML());
//$dom->save('xml/Laravel.xml');

//if (file_exists('test.xml')) {
//    $xml = simplexml_load_file('test.xml');
//} else {
//    exit('Не удалось открыть файл test.xml.');
//}

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Helper 2.0</title>
    <link rel="stylesheet" href="assets/css/main.css">
    <link href="assets/font-awesome/css/all.min.css" rel="stylesheet">
</head>

<body>

<div id="helpers">
    <h1 style="text-align: center;">Helper 2.0</h1>
    <img src="icons/add.png" id="add-button" alt="Add block" title="Add block">
    <?php foreach ($langXml as $lang): ?>
        <div id="PHP">
            <h2 class="h2-title" data-title="<?= $lang->title ?>">
                <?= $lang->title ?>
                <i id="<?= $lang->title ?>-angle-left" class="fas fa-angle-left"></i>
            </h2>

            <div id="<?= $lang->title ?>-content" class="d-none">
                <?php foreach ($lang->content as $content): ?>
                <div class="content">
                    <h3><?= $content->title ?></h3>
                    <pre><?= $content->body ?></pre>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/main.js"></script>
<script>
    $( document ).ready(function(){
        $("h2").click(function(){
            var name = $(this).attr('data-title');
            $( '#' + name + '-content').slideToggle();
            $( '#' + name + '-angle-left').toggleClass('fa-angle-left fa-angle-down');
        });
    });
</script>
</body>

</html>